1. 
select *
from (select t.name track, sum(id.amount) purchace_number
      from invoice_detail id
               join invoice i on id.invoice = i.invoice
               join track t on id.track = t.track
      group by t.name
      order by purchace_number desc, t.name)
where rownum <= 10


2. 
select distinct c.client, concat(c.name, concat(' ', c.lastname)) name, c.email
from invoice_detail id
    join invoice i on id.invoice = i.invoice
    join client c on i.client = c.client
    join track t on id.track = t.track
    join playlist_track pt on t.track = pt.track
    join playlist p on pt.playlist = p.playlist
where lower(p.name) = 'tv shows'



3.
select * from (select distinct concat(c.name, concat(' ', c.lastname)) name, c.email, sum(id.price) total
from invoice_detail id
    join invoice i on id.invoice = i.invoice
    join client c on i.client = c.client
group by concat(c.name, concat(' ', c.lastname)), c.email
order by total desc) where ROWNUM <= 10


select * from (select concat(c.name, concat(' ', c.lastname)) name, c.email, sum(i.total) total
from invoice i
    join client c on i.client = c.client
group by concat(c.name, concat(' ', c.lastname)), c.email
order by total desc) where ROWNUM <= 10



5.
select * from (select ci.description, sum(id.price) total
from invoice_detail id
    join invoice i on id.invoice = i.invoice
    join client c on i.client = c.client
    join city ci on c.city = ci.city
group by  ci.description
order by total desc) where ROWNUM <= 10;

select * from (select ci.description, sum(i.total) total
from invoice i
    join client c on i.client = c.client
    join city ci on c.city = ci.city
group by  ci.description
order by total desc) where ROWNUM <= 10;


6.
select substr(name, 0, 1)                                         letra,
       round((count(substr(name, 0, 1)) / tracks.total) * 100, 2) porcentaje
from TRACK,
     (select count(*) total from track) tracks
group by substr(name, 0, 1), tracks.total
order by porcentaje desc;


7.
select ci.description,  round((sum(id.price)/p100.tot)*100, 2) total
from invoice_detail id
    join invoice i on id.invoice = i.invoice
    join client c on i.client = c.client
    join city ci on c.city = ci.city,
    (select sum(price) tot from invoice_detail) p100
group by  ci.description, p100.tot
order by total desc;

select ci.description,  round((sum(i.total)/p100.tot)*100, 2) total
from invoice i
    join client c on i.client = c.client
    join city ci on c.city = ci.city,
    (select sum(total) tot from invoice) p100
group by  ci.description, p100.tot
order by total desc;


9.
select clts.email, clts.description, consumo, avgctls.prom
from (
         select c.email, ci.description, sum(i.total) consumo
         from invoice i
                  join client c on i.client = c.client
                  join city ci on c.city = ci.city
         where ci.description = 'Prague'
         group by c.email, ci.description
         order by consumo desc
     ) clts,
     (
         select avg(prom) prom
         from (
                  select sum(i.total) prom
                  from invoice i
                           join client c on i.client = c.client
                           join city ci on c.city = ci.city
                  where ci.description = 'Prague'
                  group by c.email, ci.description
              )
     ) avgctls
where clts.consumo > avgctls.prom


10.
select t1.client_name,
       t1.email,
       t2.description,
       t1.cantidad
from (
         select client_name, email, max(cantidad) cantidad
         from (
                  select concat(c.name, concat(' ', c.lastname)) client_name,
                         c.email,
                         count(tg.description)                   cantidad
                  from invoice_detail id
                           join invoice i on id.invoice = i.invoice
                           join client c on i.client = c.client
                           join track t on t.track = id.track
                           join track_gender tg on t.track_gender = tg.track_gender
                  group by c.email, concat(c.name, concat(' ', c.lastname)),
                           tg.description
                  order by c.EMAIL, cantidad desc)
         group by client_name, email
         order by email, cantidad desc
     ) t1
         join (select tg.description,
                      concat(c.name, concat(' ', c.lastname)) client_name,
                      c.email,
                      count(tg.description)                   cantidad
               from invoice_detail id
                        join invoice i on id.invoice = i.invoice
                        join client c on i.client = c.client
                        join track t on t.track = id.TRACK
                        join track_gender tg on t.track_gender = tg.track_gender
               group by c.email, concat(c.name, concat(' ', c.lastname)),
                        tg.description) t2 on
    t1.client_name = t2.client_name and
    t1.email = t2.email and
    t1.cantidad = t2.cantidad
    order by t2.cantidad desc, t2.email




