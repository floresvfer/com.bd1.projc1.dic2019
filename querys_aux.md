### Cantidad de clientes por Ciudad

```sql
select description, count(description) clientes
from (
         select c.email, ci.description, sum(i.total) consumo
         from invoice i
                  join client c on i.client = c.client
                  join city ci on c.city = ci.city
         group by c.email, ci.description
         order by consumo desc
     )
group by description
order by clientes desc;

6, 2, 4 - 1
```

### Porcentaje de Gastos por ciudad

```sql
select ci.description, round((sum(i.total) / p100.tot) * 100, 2) total
from invoice i
         join client c on i.client = c.client
         join city ci on c.city = ci.city,
     (select sum(total) tot from invoice) p100
group by ci.description, p100.tot
order by total desc;
```

