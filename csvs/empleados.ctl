OPTIONS (SKIP=1)
LOAD DATA
CHARACTERSET UTF8
INFILE 'empleados.csv'
INTO TABLE tmp_employee
FIELDS TERMINATED BY "," OPTIONALLY ENCLOSED BY '"'
(
    employee,
    name,
    lastname,
    boss,
    birthdate,
    hiringdate,
    address,
    city,
    phonenumber,
    email "REPLACE(:email, CHR(13),'')"
)
