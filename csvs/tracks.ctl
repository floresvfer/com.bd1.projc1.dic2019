OPTIONS (SKIP=1)
LOAD DATA
CHARACTERSET UTF8
INFILE 'tracks.csv'
INTO TABLE tmp_track
FIELDS TERMINATED BY "," OPTIONALLY ENCLOSED BY '"'
(
    name,
    duration,
    price,
    bytes,
    composer,
    album,
    artist,
    filetype,
    gender,
    list "REPLACE(:list, CHR(13),'')"
)
