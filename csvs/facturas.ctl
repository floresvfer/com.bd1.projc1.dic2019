OPTIONS (SKIP=1)
LOAD DATA
CHARACTERSET UTF8
INFILE 'facturas.csv'
INTO TABLE tmp_invoice
FIELDS TERMINATED BY "," OPTIONALLY ENCLOSED BY '"'
(
    invoice,
	song,
	album,
	artist,
	price,
	amount,
	datee,
	total,
	name,
	lastname,
	address,
	city,
	phonenumber,
	email,
	employee "REPLACE(:employee, CHR(13),'')"
)
