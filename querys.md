# Querys

1. ## Top 10 de canciones más compradas

   ```sql
   select *
   from (select t.name track, sum(id.amount) purchace_count
         from invoice_detail id
                  join invoice i on id.invoice = i.invoice
                  join track t on id.track = t.track
         group by t.name, t.artist, t.album, t.composer
         order by purchace_count desc, t.name)
   where rownum <= 10
   ```

   

2. ## Clientes que han comprado canciones de una lista determinada (se dara un nombre durante la calificacion)

   ```sql
   select distinct c.client, concat(c.name, concat(' ', c.lastname)) name, c.email
   from invoice_detail id
       join invoice i on id.invoice = i.invoice
       join client c on i.client = c.client
       join track t on id.track = t.track
       join playlist_track pt on t.track = pt.track
       join playlist p on pt.playlist = p.playlist
   where lower(p.name) = 'tv shows'
   ```

   

3. ## Top 10 Clientes que más han gastado en la tienda.

   ```sql
   select * from (select distinct concat(c.name, concat(' ', c.lastname)) name, c.email, sum(id.price) total
   from invoice_detail id
       join invoice i on id.invoice = i.invoice
       join client c on i.client = c.client
   group by concat(c.name, concat(' ', c.lastname)), c.email
   order by total desc) where ROWNUM <= 10
   ```

   ***sin sumar el detalle**

   ```sql
   select * from (select concat(c.name, concat(' ', c.lastname)) name, c.email, sum(i.total) total
   from invoice i
       join client c on i.client = c.client
   group by concat(c.name, concat(' ', c.lastname)), c.email
   order by total desc) where ROWNUM <= 10
   ```

   

4. ## Reporte de ventas totales por mes y año.

   ```sql
   select months_totals.*, year_total.total total
   from (
            select concat(year, '')                                year,
                   sum(case when month = 1 then total else 0 end)  Jan,
                   sum(case when month = 2 then total else 0 end)  Feb,
                   sum(case when month = 3 then total else 0 end)  Mar,
                   sum(case when month = 4 then total else 0 end)  Apr,
                   sum(case when month = 5 then total else 0 end)  May,
                   sum(case when month = 6 then total else 0 end)  Jun,
                   sum(case when month = 7 then total else 0 end)  Jul,
                   sum(case when month = 8 then total else 0 end)  Aug,
                   sum(case when month = 9 then total else 0 end)  Sep,
                   sum(case when month = 10 then total else 0 end) Oct,
                   sum(case when month = 11 then total else 0 end) Nov,
                   sum(case when month = 12 then total else 0 end) Dec
            from (
                     select extract(year from invoice_date)  year,
                            extract(month from invoice_date) month,
                            sum(total)                       total
                     from invoice
                     group by extract(month from invoice_date), extract(year from invoice_date)
                     order by year asc
                 )
            group by year) months_totals
            join (
       select extract(year from invoice_date) year,
              sum(total)                      total
       from invoice
       group by extract(year from invoice_date)
       order by year asc
   ) year_total on year_total.year = months_totals.year
   
   union
   
   select month_global.*, global.total
   from (
            select 'ZTOTAL'                                        year,
                   sum(case when month = 1 then total else 0 end)  Jan,
                   sum(case when month = 2 then total else 0 end)  Feb,
                   sum(case when month = 3 then total else 0 end)  Mar,
                   sum(case when month = 4 then total else 0 end)  Apr,
                   sum(case when month = 5 then total else 0 end)  May,
                   sum(case when month = 6 then total else 0 end)  Jun,
                   sum(case when month = 7 then total else 0 end)  Jul,
                   sum(case when month = 8 then total else 0 end)  Aug,
                   sum(case when month = 9 then total else 0 end)  Sep,
                   sum(case when month = 10 then total else 0 end) Oct,
                   sum(case when month = 11 then total else 0 end) Nov,
                   sum(case when month = 12 then total else 0 end) Dec
            from (
                     select extract(month from invoice_date) month, sum(total) total
                     from invoice
                     group by extract(month from invoice_date)
                 )
        ) month_global,
        (
            select sum(total) total
            from invoice
        ) global;
   ```

   

5. ## Top 10 de ciudades que más han gastado en la tienda.

   ```sql
   select * from (select ci.description city, sum(id.price) total
   from invoice_detail id
       join invoice i on id.invoice = i.invoice
       join client c on i.client = c.client
       join city ci on c.city = ci.city
   group by  ci.description
   order by total desc) where ROWNUM <= 10;
   ```

   ***sin sumar el detalle**

   ```sql
   select * from (select ci.description, sum(i.total) total
   from invoice i
       join client c on i.client = c.client
       join city ci on c.city = ci.city
   group by  ci.description
   order by total desc) where ROWNUM <= 10;
   ```

   

6. ## Porcentaje de canciones por la letra que empieza.

   ```sql
   select substr(name, 0, 1)                                         letter,
          round((count(substr(name, 0, 1)) / tracks.total) * 100, 2) porcent
   from TRACK,
        (select count(*) total from track) tracks
   group by substr(name, 0, 1), tracks.total
   order by porcent desc;
   ```

   

7. ## Porcentaje de ventas de cada ciudad, en el 100% representan todos los ingresos de la tienda.

   ```sql
   select ci.description city,  round((sum(id.price)/p100.tot)*100, 2) sales_percent
   from invoice_detail id
       join invoice i on id.invoice = i.invoice
       join client c on i.client = c.client
       join city ci on c.city = ci.city,
       (select sum(price) tot from invoice_detail) p100
   group by  ci.description, p100.tot
   order by sales_percent desc;
   ```

   ***sin sumar el detalle**

   ```sql
   select ci.description,  round((sum(i.total)/p100.tot)*100, 2) total
   from invoice i
       join client c on i.client = c.client
       join city ci on c.city = ci.city,
       (select sum(total) tot from invoice) p100
   group by  ci.description, p100.tot
   order by total desc;
   ```

   

8. ## Mostrar el porcentaje de las categorías más vendidas de cada ciudad de la siguiente manera: Ciudad, Género, Porcentaje De mercado.

   ```sql
   select t1.city,
          t1.gender,
          round(((t1.songs / t2.tot) * 100), 2)         city_percent_eq,
          round(((t1.songs / t1.songs_total) * 100), 2) global_percent_eq
   from (
            select row_number()
                           over (partition by ci.description order by sum(id.amount) desc) indx,
                   ci.description                                                          city,
                   tg.description                                                          gender,
                   sum(id.AMOUNT)                                                          songs,
                   total.tot                                                               songs_total
            from invoice i
                     join client c on i.client = c.client
                     join city ci on c.city = ci.city
                     join invoice_detail id on i.invoice = id.invoice
                     join track t on id.track = t.track
                     join track_gender tg on t.track_gender = tg.track_gender,
                 (select sum(amount) tot from invoice_detail) total
            group by tg.description, ci.description, total.tot
        ) t1,
        (select sum(id.amount) tot, ci.description city
         from invoice i
                  join client c on i.client = c.client
                  join city ci on c.city = ci.city
                  join invoice_detail id on i.invoice = id.invoice
         group by ci.description) t2
   where t1.city = t2.city
     and indx <= 3
   order by city, songs desc
   ```

   

9. ## Mostrar los clientes que hayan consumido más que el promedio que consumen una ciudad determinada. (se dará un nombre de durante la calificación).

   ```sql
   select clts.name, clts.email, clts.description client_city, consumption, avgctls.prom selected_city_avg
   from (
            select concat(c.name, concat(' ', c.lastname)) name, c.email, ci.description, sum(i.total) consumption
            from invoice i
                     join client c on i.client = c.client
                     join city ci on c.city = ci.city
            group by c.email, ci.description, concat(c.name, concat(' ', c.lastname))
            order by consumption desc
        ) clts,
        (
            select avg(prom) prom
            from (
                     select sum(i.total) prom
                     from invoice i
                              join client c on i.client = c.client
                              join city ci on c.city = ci.city
                     where ci.description = 'Paris'
                     group by c.email, ci.description
                 )
        ) avgctls
   where clts.consumption > avgctls.prom
   ```
   
   ```sql
   select clts.email, clts.description, consumo, avgctls.prom
   from (
            select c.email, ci.description, sum(i.total) consumo
            from invoice i
                     join client c on i.client = c.client
                     join city ci on c.city = ci.city
            group by c.email, ci.description
            order by consumo desc
        ) clts,
        (
            select avg(prom) prom
            from (
                     select sum(i.total) prom
                     from invoice i
                              join client c on i.client = c.client
                              join city ci on c.city = ci.city
                     where ci.description = 'Paris'
                     group by c.email, ci.description
                 )
        ) avgctls
   where clts.consumo > avgctls.prom
   ```
   
   
   
10. ## Mostrar el género que más compra cada cliente.

    ```sql
    select t1.client_name,
           t1.email,
           t2.description gender,
           t1.cantidad amount
    from (
             select client_name, email, max(cantidad) cantidad
             from (
                      select concat(c.name, concat(' ', c.lastname)) client_name,
                             c.email,
                             count(tg.description)                   cantidad
                      from invoice_detail id
                               join invoice i on id.invoice = i.invoice
                               join client c on i.client = c.client
                               join track t on t.track = id.track
                               join track_gender tg on t.track_gender = tg.track_gender
                      group by c.email, concat(c.name, concat(' ', c.lastname)),
                               tg.description
                      order by c.EMAIL, cantidad desc)
             group by client_name, email
             order by email, cantidad desc
         ) t1
             join (select tg.description,
                          concat(c.name, concat(' ', c.lastname)) client_name,
                          c.email,
                          count(tg.description)                   cantidad
                   from invoice_detail id
                            join invoice i on id.invoice = i.invoice
                            join client c on i.client = c.client
                            join track t on t.track = id.TRACK
                            join track_gender tg on t.track_gender = tg.track_gender
                   group by c.email, concat(c.name, concat(' ', c.lastname)),
                            tg.description) t2 on
        t1.client_name = t2.client_name and
        t1.email = t2.email and
        t1.cantidad = t2.cantidad
        order by t2.cantidad desc, t2.email
    ```

    

11. ## Mostrar los empleados con la cantidad de personas a cargo y su jefe inmediato.

    ```sql
    select
           concat(emp.name, concat(' ', emp.lastname)) employee,
           concat(boss.name, concat(' ', boss.lastname)) boss,
           count(c.client) support_clients
    from employee_boss eb
        join employee boss on boss.employee = eb.boss
        join employee emp on emp.employee = eb.employee
        join support_employee se on se.employee = emp.employee
        join client c on c.client = se.client
    group by boss.name, boss.lastname, emp.name, emp.lastname
    order by support_clients desc;
    ```

    

12. ## Top 5 de clientes que más compran de cada ciudad con el porcentaje que representan en esa ciudad.

    ```sql
    select
           t1.mail,
           t1.name,
           t1.city,
           t1.consumption client_consumption,
           t2.tot city_consumption,
           round(((t1.consumption / t2.tot) * 100), 2)         city_percent_eq,
           t1.consumption_total global_consumption,
           round(((t1.consumption / t1.consumption_total) * 100), 2) global_percent_eq
    from (
             select row_number()
                            over (partition by ci.description order by sum(i.total) desc) indx,
                    ci.description                                                          city,
                    c.email                                                                 mail,
                    concat(c.name, concat(' ', c.lastname))                                 name,
                    c.client                                                                clientid,
                    sum(i.total)                                                            consumption,
                    total.tot                                                               consumption_total
             from invoice i
                      join client c on i.client = c.client
                      join city ci on c.city = ci.city,
                  (select sum(total) tot from invoice) total
             group by c.client, c.email, concat(c.name, concat(' ', c.lastname)), ci.description, total.tot
         ) t1,
         (select sum(i.total) tot, ci.description city
          from invoice i
                   join client c on i.client = c.client
                   join city ci on c.city = ci.city
          group by ci.description) t2
    where t1.city = t2.city
      and indx <= 5
    order by city, consumption desc
    ```

    